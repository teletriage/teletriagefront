import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },

  {
    path: '/help',
    name: 'Help',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Help.vue')
  },
  {
    path: '/diagnostic',
    name: 'diagnostic',
    meta: { requiresAuth: true },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Diagnostic.vue')
  },
  {
    path: '/diagnostic/symptoms',
    name: 'symptoms',
    meta: { requiresAuth: true },
    component: () => import(/* webpackChunkName: "about" */ '../views/Symptoms.vue'),
  },
  {
    path: '/diagnostic/init-questions',
    name: 'init-questions',
    meta: { requiresAuth: true },
    component: () => import(/* webpackChunkName: "about" */ '../views/initQuestions'),
  },
  {
    path: '/diagnostic/questions',
    name: 'questions',
    meta: { requiresAuth: true },
    component: () => import(/* webpackChunkName: "about" */ '../views/Questions.vue'),
  },
  {
    path: '/diagnostic/result',
    name: 'result',
    meta: { requiresAuth: true },
    component: () => import(/* webpackChunkName: "about" */ '../views/Result.vue'),
  },
  {
    path: '/credits',
    name: 'Creditos',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Credits.vue')
  }
]
 

const router = new VueRouter({
  mode: "history",
  routes,
  scrollBehavior() {
    document.getElementById('app').scrollIntoView();
  }
})

//EJEMPLO DE COMO USAR EL ROUTER
// const router = new VueRouter({
//     mode: 'history',
//     routes: [
//         { path: '/', component: componentInicio },
//         { path: '/usuario/login', component: componentUsuarioLogin, meta: { guest: true } },
//         { path: '/usuario/registro', component: componentUsuarioRegistro, meta: { guest: true } },
//         { path: '/consulta', component: componentConsulta, meta: { requiresAuth: true } },
//         { path: '/consulta/pregunta', component: componentConsultaPregunta, meta: { requiresAuth: true } },
//         { path: '/consulta/resultado', component: componentConsultaResultado, meta: { requiresAuth: true } },
//         { path: '*', component: componentError404 }
//     ]
// });

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem('access_token') == null) {
      next({
        path: '/',
        params: { nextUrl: to.fullPath }
      })
    } else {
      //let user = JSON.parse(localStorage.getItem('user'))
      /* if (to.matched.some(record => record.meta.is_admin)) {
          if (user.is_admin == 1) {
              next()
          }
          else {
              next({ name: 'userboard' })
          }
      } else {
          next()
      } */
      next()
    }
    next()
  } else if (to.matched.some(record => record.meta.guest)) {
    // if (localStorage.getItem('jwt') == null) {
    //     next()
    // }
    // else {
    //     next({ name: 'userboard' })
    // }
    console.log("NO PIDE SER USUARIO LOGIN PAPU")
    next()
  } else {
    //console.log("CUALQUIERA")
    next()
  }
})

export default router
