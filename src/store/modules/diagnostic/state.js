export default {
  questions: [],
  init_questions:{
    questionsR:{
      1:undefined,
      2:undefined,
      3:undefined,
      5:undefined,
    },
    dateValue: "",
    questionsI:[]
  },
  checkedresponses: [],
  responses: [],
  patient: {
    city: null,
    agecategory: null,
    age:null,
    gender: null,
    eps: null,
    location_id:0,
    agerange:null
  },
  companies: [],
  localities: [],
  specialties: [],
  municipalities: [],
  symptomsselected: "",
  symptoms: [],
  symptomstemp: [],
};
