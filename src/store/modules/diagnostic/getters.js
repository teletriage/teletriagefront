export const questions = (state) => {
  if (state.questions) {
    return state.questions
      .filter(function(q) {
        return q.question_category.title == state.symptomsselected;
      })
      .filter(function(q2) {
        return q2.age_range.title == state.patient.agecategory;
      });
  } else {
    return [];
  }
};

export const init_questions=state=>{
    return state.init_questions;
}

export const companies = (state) => {
  if (state.companies) {
    return state.companies;
  } else {
    return [];
  }
};

export const specialties = (state) => {
  if (state.specialties) {
    return state.specialties;
  } else {
    return [];
  }
};

export const specialtiesIndex = (state) => {
  if (state.symptomsselected) {
    return state.symptomstemp.indexOf(state.symptomsselected);
  } else {
    return -1;
  }
};

export const localities = (state) => {
  if (state.localities) {
    return state.localities;
  } else {
    return [];
  }
};

export const municipalities = (state) => {
  if (state.municipalities) {
    return state.municipalities;
  } else {
    return [];
  }
};

export const symptoms = (state) => {
  return state.symptoms;
};

export const symptomsselected = (state) => {
  return state.symptomsselected;
};

export const patient = (state) => {
  return state.patient;
};

export const symptomstemp = (state) => {
  return state.symptomstemp;
};

export const checkedresponses = (state) => {
  return state.checkedresponses;
};
