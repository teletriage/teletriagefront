export const questions = (state, data) => {
  state.questions = data.data;
};

export const init_questions= (state, data)=>{
  state.init_questions=data
};

export const companies = (state, data) => {
  state.companies = data.data;
};

export const localities = (state, data) => {
  state.localities = data.data;
};

export const municipalities = (state, data) => {
  state.municipalities = data.data;
};

export const specialties = (state, data) => {
  state.specialties = data.data;
};

export const patient = (state, data) => {
  state.patient = data;
};

export const questionsCategory=(state, data)=>{
  state.symptoms=data.data.map(category=>category.title).filter(function (value) { return value!=='Noxa de contagio'});
  console.log(data.data.map(category=>category.title).filter(function (value) {return value!=='Noxa de contagio'}))
  state.symptomstemp=data.data.map(category=>category.title).filter(function (value) {return value!=='Noxa de contagio'});
}

export const noxa=(state, data)=>{
  state.symptomsselected = data;
}

export const symptoms = (state, data) => {
  state.symptomstemp = [
    data,
    ...state.symptoms.filter(function(ele) {
      return ele != data;
    }),
  ];
  state.symptomsselected = data;
};

export const responses = (state, data) => {
  state.responses.push(data);
};

export const checkedresponses = (state, data) => {
  state.checkedresponses = data;
};

export const changeSymptom = (state, dir) => {
  var indexselected = state.symptomstemp.indexOf(state.symptomsselected);
  var lengthlista = state.symptomstemp.length;
  if (dir == 1) {
    if (indexselected + 1 < lengthlista) {
      state.symptomsselected = state.symptomstemp[indexselected + 1];
    }
  } else if (dir == 0) {
    if (indexselected > 0) {
      state.symptomsselected = state.symptomstemp[indexselected - 1];
    }
  }
};

export const finish = (state) => {
  state.patient = {
    city: null,
    agecategory: null,
    gender: null,
    eps: null,
    location_id:0,
    agerange:null
  };
  state.init_questions={
    questionsR:{
      1:undefined,
      2:undefined,
      3:undefined,
      5:undefined,
    },
    dateValue: ""
  }
};
