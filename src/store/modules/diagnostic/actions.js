import axios from "axios";
axios.defaults.headers.common["Authorization"] = localStorage.getItem(
  "access_token"
);
axios.defaults.headers.post["Content-Type"] =
  "application/x-www-form-urlencoded";

const apiURL = require("../../../config.js").apiURL;

export const getQuestions = ({ commit }) => {
  return new Promise((resolve, reject) => {
    axios({
      method: "get",
      url: apiURL + "question?limit=1000",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("access_token"),
      },
    })
      .then((response) => {
        commit("questions", response.data);
        resolve(1);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const getQuestionsCategory = ({ commit }) => {
  return new Promise((resolve, reject) => {
    axios({
      method: "get",
      url: apiURL + "questioncategory",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("access_token"),
      },
    })
      .then((response) => {
        commit("questionsCategory", response.data);
        resolve(1);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const getCompanies = ({ commit }, data) => {
  return new Promise((resolve, reject) => {
    var url = "companies?query=";
    if (data && data.query) {
      url = url + data.query + "";
    }
    axios({
      method: "get",
      url: apiURL + url,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("access_token"),
      },
    })
      .then((response) => {
        commit("companies", response.data);
        resolve(1);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const getSpecialties = ({ commit }) => {
  return new Promise((resolve, reject) => {
    axios({
      method: "get",
      url: apiURL + "specialties?limit=5000000",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("access_token"),
      },
    })
      .then((response) => {
        commit("specialties", response.data);
        resolve(1);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const getLocalities = ({ commit }) => {
  return new Promise((resolve, reject) => {
    axios({
      method: "get",
      url: apiURL + "localities?limit=50",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("access_token"),
      },
    })
      .then((response) => {
        commit("localities", response.data);
        resolve(1);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const getMunicipalities = ({ commit }) => {
  return new Promise((resolve, reject) => {
    axios({
      method: "get",
      url: apiURL + "municipalities?limit=500000",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("access_token"),
      },
    })
      .then((response) => {
        commit("municipalities", response.data);
        resolve(1);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const setPatient = ({ commit }, data) => {
  return new Promise((resolve) => {
    axios({
      method: "get",
      url: apiURL + "userAction",
      params: {
        title: "Inicio Diagnostico",
      },
    }).then((response) => {
      console.log(response.data);
      axios
        .post(apiURL + "userLogs", {
          user_id: localStorage.getItem("user_id"),
          user_action_id: response.data[0].id,
        })
        .then((message) => {
          console.log(message);
          commit("patient", data);
          resolve(1);
        });
    });
  });
};

export const finish = ({ commit }, data) => {
  return new Promise((resolve, reject) => {
    axios
      .post(apiURL + "patientLogs", {
        user_id: data.user_id,
        locality_id: data.locality_id,
        municipality_id: data.municipality_id,
        age_range_id: data.age_range_id,
        age: data.age,
        symptoms_origin_date: data.symptoms_origin_date,
        gender: data.gender,
        company_id: data.company_id,
        result_action_id: data.result_action_id,
      })
      .then((response1) => {
        console.log(response1.data);
        axios
          .post(apiURL + "patientLogSymptom", {
            patient_log_id: response1.data.PatientLogId,
            symptoms: data.symptoms,
          })
          .then((response2) => {
            console.log(response2.data);
            axios({
              method: "get",
              url: apiURL + "userAction",
              params: {
                title: "Finalización de Diagnostico",
              },
            }).then((response) => {
              console.log(response.data[0].id);
              axios
                .post(apiURL + "userLogs", {
                  user_id: data.user_id,
                  user_action_id: response.data[0].id,
                })
                .then((response3) => {
                  console.log(response3.data);
                  commit("finish");
                  resolve(1);
                })
                .catch((error) => {
                  reject(error);
                });
            });
          })
          .catch((error) => {
            reject(error);
          });
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const setResponses = ({ commit }, data) => {
  return new Promise((resolve) => {
    commit("responses", data);
    resolve(1);
  });
};

export const setCheckedResponses = ({ commit }, data) => {
  return new Promise((resolve) => {
    commit("checkedresponses", data);
    resolve(1);
  });
};

export const setNoxa = ({ commit }, data) => {
  return new Promise((resolve) => {
    commit("noxa", data);
    resolve(1);
  });
};

export const setSymptoms = ({ commit }, data) => {
  return new Promise((resolve) => {
    commit("symptoms", data);
    resolve(1);
  });
};

export const setInit_questions = ({ commit }, data) => {
  return new Promise((resolve) => {
    commit("init_questions", data);
    resolve(1);
  });
};

export const changeSymptom = ({ commit }, dir) => {
  return new Promise((resolve) => {
    commit("changeSymptom", dir);
    resolve(1);
  });
};
