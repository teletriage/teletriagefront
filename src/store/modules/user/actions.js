import axios from "axios";
axios.defaults.headers.common["Authorization"] = localStorage.getItem(
  "access_token"
);
axios.defaults.headers.post["Content-Type"] =
  "application/x-www-form-urlencoded";

const apiURL = require("../../../config.js").apiURL;

export const login = ({ commit }, data) => {
  let token = "";
  let expires_at = "";
  let token_type = "";
  let user_email = "";
  let id = "";
  return new Promise((resolve, reject) => {
    axios
      .post(apiURL + "auth/login", {
        email: data.email,
        password: data.password,
        remember_me: data.rememberMe,
      })
      .then((response) => {
        token = response.data.access_token;
        expires_at = response.data.expires_at;
        token_type = response.data.token_type;
        user_email = data.email;
        const config = {
          headers: { Authorization: `Bearer ${token}` },
        };
        axios.get(`${apiURL}auth/user`, config).then((r) => {
          id = r.data.id;
          localStorage.setItem("access_token", token);
          localStorage.setItem("expires_at", expires_at);
          localStorage.setItem("token_type", token_type);
          localStorage.setItem("user_email", user_email);
          localStorage.setItem("user_id", id);
          commit("login", {
            access_token: token,
            expires_at: expires_at,
            token_type: token_type,
            user_email: user_email,
            user_id: id,
          });
          resolve(1);
        });
      })
      .catch((error) => {
        console.log(error.response.data);
        reject(error.response.data);
      });
  });
};

export const logout = ({ commit }) => {
  return new Promise((resolve) => {
    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
      },
    };
    axios.get(`${apiURL}auth/logout`, config).then(() => {
      localStorage.clear();
      commit("logout");
      resolve(1);
    })
    .catch((error) => {
        console.log(error);
    });
  });
};

export const signUp = ({ commit }, data) => {
  console.log(data);
  return new Promise((resolve, reject) => {
    axios
      .post(apiURL + "auth/signup", data)
      .then((response) => {
        commit("singUp", response.data);
      })
      .catch((error) => {
        reject(error.response.data);
      });
  });
};

export const getDocumentTypes = ({ commit }) => {
  return new Promise((resolve) => {
    axios.get(apiURL + "documentTypes").then((response) => {
      console.log(response.data.data);
      commit("documentTypes", response.data.data);
      resolve(1);
    });
  });
};
