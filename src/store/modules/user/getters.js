export const isAuth = state => {
    return state.access_token != '' && state.access_token != null;
}

export const user_email= state=>{
    return state.user_email;
}

export const user_id= state=>{
    return state.user_id
}

export const documentTypes=state=>{
    return state.document_types
}
