export default {
    access_token: localStorage.getItem('access_token'),
    expires_at: localStorage.getItem('expires_at'),
    token_type: localStorage.getItem('token_type'),
    user_email:localStorage.getItem('user_email'),
    user_id:localStorage.getItem('user_id'),
    document_types:[]
}
