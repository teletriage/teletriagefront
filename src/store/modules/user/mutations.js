export const login = (state, data) => {
    state.access_token = data.access_token;
    state.expires_at = data.expires_at;
    state.token_type = data.token_type;
    state.user_email= data.user_email;
    state.user_id=data.user_id
}

export const documentTypes = (state, data) => {
    console.log(data)
    state.document_types = data;
};

export const singUp=(state,data)=>{
    console.log(data)
}

export const logout = (state) => {
    state.access_token = '';
    state.expires_at = '';
    state.token_type = '';
}
