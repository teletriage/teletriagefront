import Vue from "vue";
import "./plugins/fontawesome";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vSelect from "vue-select";

Vue.component("v-select", vSelect);

import BootstrapVue from "bootstrap-vue";
import Vuelidate from "vuelidate/src";

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

import "./assets/css/style.css";
import "./assets/css/nice-select.css";
import "./assets/css/magnific-popup.css";
import "./assets/css/nice-select.css";
import "./assets/css/animate.css";
import "./assets/css/slicknav.css";

import "vue-select/dist/vue-select.css";

window.config = require("./config");

Vue.use(BootstrapVue);
Vue.use(Vuelidate);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
