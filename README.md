# teletriagefront

## Project setup
```
npm install
```

## Instalación con contenedor para producción

Para instalar un contenedor de la aplicación en un ambiente de producción seguir los siguientes pasos:

### Clonar el repositorio
```Shell
git clone https://gitlab.com/teletriage/teletriagefront.git front
```

### Moverse al directorio de la app
```Shell
cd front
```

### Agregar la ruta del API del Back-End en el archivo src/config.js
```JavaScript
module.exports = {
    apiURL: ''
}
```

### Construir el contenedor
```Shell
sudo docker build . -t teletriage-front -f Dockerfile
```

### Ejecutar desde Docker
```Shell
sudo docker run -it -p 8080:80 -d --rm --name teletriage teletriage-front
```

---

## Generacion de ambientes para desarrollo y producción

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


